import json

with open("patient.json") as in_file:
    js_string = in_file.read()

js_data = json.loads(js_string)

family_name = ""
first_name = ""

for js_key in js_data:
    if "identifier" in js_key:
        id_dict = js_data[js_key][0]
        for id_key in id_dict:
            if "name" in id_key:
                whole_name = id_dict[id_key][0]
                if "family" in whole_name:
                    family_name = whole_name['family'][0]
                if "given" in whole_name:
                    given_name = whole_name['given'][0]
            elif "managingOrganization" in id_key:
                org_dict = ide_dict[id_key]
                if "display" in org_dict:
                    org_name = org_dict["display"]
            elif "gender" in id_key:
                gender = id_dict[id_key]
            elif "conditions" in id_key:
                conditions = id_dict[id_key]

print(family_name)
print(given_name)
print(org_name)

print("Name of patient: {} {}".format(given_name, family_name))
print("Organization name: {}".format(org_name))
print("Gender: {}".format(gender))
print("Numer of conditions they have: {}".format(len(conditions)))
print("List of all conditions:")
for item in conditions:
    print("-{}".format(item))